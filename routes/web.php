<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('login/{provider}', 'SocialiteCtrl@redirectToProvider')->name('socialite');
Route::get('login/{provider}/callback', 'SocialiteCtrl@handleProviderCallback')->name('socialite.callback');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
