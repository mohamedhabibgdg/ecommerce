<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder{

    public function run(){

        factory(\App\User::class)->create([
            "email"=>"admin@app.com",
            "is_admin"=>true,
            "setting"=>["color"=>"red"]
        ]);

    }

}
