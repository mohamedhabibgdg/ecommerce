<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialitesTable extends Migration{

    public function up(){
        Schema::create('socialites', function (Blueprint $table) {
            $table->id();
            $table->string('provider');//google facebook github
            $table->string('provider_id');
            $table->text('provider_data');
            $table->unsignedBigInteger('user_id');
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    public function down() { Schema::dropIfExists('socialites'); }

}
