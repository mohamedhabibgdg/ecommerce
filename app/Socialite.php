<?php

namespace App;

use App\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Socialite extends Model{
    use SoftDeletes,HasUser;
    protected $table="socialites";
    protected $casts=[
        "provider_data"=>"collection"
    ];

    protected $guarded=[''];

}
