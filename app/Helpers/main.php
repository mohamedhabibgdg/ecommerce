<?php


use Illuminate\Http\UploadedFile;

if (!function_exists('uploader')){
    function uploader($file,$folder="uploader",array $validation=[]){
        $folder='uploads/'.rtrim($folder,'/');
        if (!$file instanceof UploadedFile){
            request()->validate(
                ($validation===[])?[
                    $file=>["required",'mimes:jpeg,png','max:2048']
                ]:$validation
            );
            $file=request()->file($file);
        }
        $name=uniqid().$file->getClientOriginalExtension();
        $file->move(public_path($folder),$name);

        return $folder.'/'.$name;
    }
}
