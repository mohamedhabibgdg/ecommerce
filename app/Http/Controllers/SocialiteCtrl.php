<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\User;
use App\Socialite as SocialiteModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class SocialiteCtrl extends Controller{
    protected $redirectTo = RouteServiceProvider::HOME;
    public function redirectToProvider($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
        $getInfo = Socialite::driver($provider)->user();

        $user=User::firstOrCreate(['email'=>$getInfo->email],[
            "name"=>$getInfo->name ?? $getInfo->nickname ,
            "email_verified_at"=>now(),
            "password"=>Str::random(32)
        ]);

        SocialiteModel::firstOrCreate([
            "user_id"=>$user->id,
            "provider"=>$provider,
            "provider_id"=>$getInfo->id,
        ],[
            "provider_data"=>$getInfo
        ]);

        auth()->login($user);

        return redirect()->to($this->redirectTo);
    }

}
