<?php


namespace App\Traits;


use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait HasUser{

    public static function UserIdFiled(){
        return "user_id";
    }

    public static function bootHasUser(){
        static::creating(function (Model $model){
            $model->{static::UserIdFiled()} = $model->{static::UserIdFiled()} ?? auth()->id();
        });
    }

    public function user(){
        return $this->belongsTo(User::class,static::UserIdFiled());
    }

    public function scopeOwn(Builder $query,$user_id=null){
        return $query->where(static::UserIdFiled(),$user_id ?? auth()->id());
    }
}
