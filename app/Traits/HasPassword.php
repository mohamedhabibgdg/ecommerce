<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

trait HasPassword {

    public static function PasswordFiled(){
        return "password";
    }

    public static function bootHasPassword(){

        static::creating(function (Model $model){
            $model->{static::PasswordFiled()} = bcrypt($model->{static::PasswordFiled()});
        });

        static::updating(function (Model $model){
            $model->{static::PasswordFiled()} = bcrypt($model->{static::PasswordFiled()});
        });

    }

    public function comparePassword(string $password):bool {
        return Hash::check($password,$this->{static::PasswordFiled()});
    }

}
