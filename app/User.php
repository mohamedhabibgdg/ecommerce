<?php

namespace App;

use App\Traits\HasPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail{
    use Notifiable,HasPassword,SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','birthdate','is_admin','setting','email_verified_at'
    ];

    protected $appends=[
        'age'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthdate' => 'datetime:Y-m-d',
        'is_admin'=>'boolean',
        'setting'=>'collection'
    ];

    public function getAgeAttribute(){ return now()->format('Y') - $this->birthdate->format('Y'); }

    public function socials(){
        return $this->hasMany(Socialite::class,'user_id');
    }
}
